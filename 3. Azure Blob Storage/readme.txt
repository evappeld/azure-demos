// *************************************************************************************************************************
// Instructions: This sample can be run using either the Azure Storage Emulator that installs as part of this SDK - or by
// updating the App.Config file with your AccountName and Key. 
// 
// To run the sample using the Storage Emulator (default option)
//      1. Start the Azure Storage Emulator (once only) by pressing the Start button or the Windows key and searching for it
//         by typing "Azure Storage Emulator". Select it from the list of applications to start it.
//      2. Set breakpoints and run the project using F10. 
// 
// To run the sample using the Storage Service
//      1. Open the app.config file and comment out the connection string for the emulator (UseDevelopmentStorage=True) and
//         uncomment the connection string for the storage service (AccountName=[]...)
//      2. Create a Storage Account through the Azure Portal and provide your [AccountName] and [AccountKey] in 
//         the App.Config file. See http://go.microsoft.com/fwlink/?LinkId=325277 for more information
//      3. Set breakpoints and run the project using F10. 
// 
// *************************************************************************************************************************

﻿using Microsoft.WindowsAzure.Management.Sql.Models;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace _7.Azure_SQL_Database
{
    class Program
    {
        private static string _serverName;

        static void Main(string[] args)
        {
            // This Quickstart demonstrates using WAML how to provision a new Microsoft Azure SQL DB Server,
            // Configure the Firewall Rules, Create a new Database, Then drop the Database and Delete the Server.

            // 1. Download your Microsoft Azure PublishSettings file; to do so click here:
            //    http://go.microsoft.com/fwlink/?LinkID=276844 

            // 2. Fill in the [PATH] + [FILENAME] of the PublishSettings file below in PublishSettingsFilePath.

            // 3. Fill in the [ACCOUNT NAME], [CONTAINER NAME], [FILE NAME] in order to Export a Database to blob storage.

            // 4. Choose an [ADMIN USER] and [ADMIN PASSWORD] that you wish to use for the server.

            // 5. Fill in [FIREWALL RULE START] & [FIREWALL RULE END]


            var parameters = new SqlManagementControllerParameters
            {
                PublishSettingsFilePath = @"C:\temp\centric.publishsettings",
                ServerRegion = "West Europe",
                ServerAdminUsername = "erik",
                ServerAdminPassword = "Pa$$w0rd",
                FirewallRuleAllowAzureServices = true,
                FirewallRuleName = "Local IP",
                FirewallRuleStartIP = "0.0.0.0",
                FirewallRuleEndIP = "255.255.255.254", // Example Firewall Rule only. Do Not Use in Production.
                DatabaseName = "CentricDemo",
                DatabaseEdition = "Web",
                DatabaseMaxSizeInGB = 1,
                DatabaseCollation = "SQL_Latin1_General_CP1_CI_AS"
            };

            Task.WaitAll(SetupAndTearDownLogicalSQLServer(parameters));

            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static async Task SetupAndTearDownLogicalSQLServer(SqlManagementControllerParameters parameters)
        {
            int step = 1;

            await SetupServerAsync(parameters, step++);
            await ListServersAsync(parameters, step++);
            await ConfigureFirewallAsync(parameters, step++);
            await ListFirewallRulesAsync(parameters, step++);
            await CreateDatabaseAsync(parameters, step++);
            await ListDatabasesAsync(parameters, step++);
            await TearDownDatabaseAsync(parameters, step++);
            await TearDownServerAsync(parameters, step++);
        }

        private static async Task ListDatabasesAsync(SqlManagementControllerParameters parameters, int step)
        {
            using (var controller = new SqlManagementController(parameters.PublishSettingsFilePath))
            {
                Console.WriteLine("\n{1}. Listing Databases on Server {0}", _serverName, step);
                ConsoleContinuePrompt("List");

                var t = Task<DatabaseListResponse>.Run(() => { return controller.ListDatabasesAsync(_serverName); });
                WaitForStatus(t);

                var databases = from s
                              in t.Result.Databases
                                select s.Name;

                Console.Write("\n");

                foreach (string database in databases)
                {
                    Console.WriteLine("   Database - {0}", database);
                }

                Console.WriteLine("...Complete");
            }
        }

        private static async Task ListFirewallRulesAsync(SqlManagementControllerParameters parameters, int step)
        {
            using (var controller = new SqlManagementController(parameters.PublishSettingsFilePath))
            {
                Console.WriteLine("\n{1}. Listing Firewall Rules for Server {0}", _serverName, step);
                ConsoleContinuePrompt("List");

                var t = Task<FirewallRuleListResponse>.Run(() => { return controller.ListFirewallRulesAsync(_serverName); });
                WaitForStatus(t);

                var rules = from r
                            in t.Result.FirewallRules
                            select new
                            {
                                Name = r.Name,
                                StartIP = r.StartIPAddress,
                                EndIP = r.EndIPAddress
                            };

                Console.Write("\n");

                foreach (var rule in rules)
                {
                    Console.WriteLine("   Rule - {0}\tStart IP - {1}\tEnd IP - {2}", rule.Name, rule.StartIP, rule.EndIP);
                }

                Console.WriteLine("...Complete");
            }
        }

        private static async Task ListServersAsync(SqlManagementControllerParameters parameters, int step)
        {
            using (var controller = new SqlManagementController(parameters.PublishSettingsFilePath))
            {
                Console.WriteLine("\n{1}. Listing Servers", _serverName, step);
                ConsoleContinuePrompt("List");

                var t = Task<ServerListResponse>.Run(() => { return controller.ListServersAsync(); });
                WaitForStatus(t);

                var servers = from s
                              in t.Result.Servers
                              select s.Name;

                Console.Write("\n");

                foreach (string server in servers)
                {
                    Console.WriteLine("   Server - {0}", server);
                }

                Console.WriteLine("...Complete");
            }
        }

        private static async Task TearDownServerAsync(SqlManagementControllerParameters parameters, int step)
        {
            using (var controller = new SqlManagementController(parameters.PublishSettingsFilePath))
            {
                Console.WriteLine("\n{1}. Dropping Server {0}", _serverName, step);
                ConsoleContinuePrompt("Drop");

                Task t = Task.Run(() => controller.DeleteServerAsync(_serverName));
                WaitForStatus(t);

                Console.WriteLine("\n...Complete");
            }
        }
        private static async Task TearDownDatabaseAsync(SqlManagementControllerParameters parameters, int step)
        {
            using (var controller = new SqlManagementController(parameters.PublishSettingsFilePath))
            {
                Console.WriteLine("\n{2}. Dropping Database {1} on Server {0}", _serverName, parameters.DatabaseName, step);
                ConsoleContinuePrompt("Drop");

                Task t = Task.Run(() => controller.DropDatabaseAsync(_serverName, parameters.DatabaseName));
                WaitForStatus(t);

                Console.WriteLine("\n...Complete");
            }
        }
        private static async Task CreateDatabaseAsync(SqlManagementControllerParameters parameters, int step)
        {
            using (var controller = new SqlManagementController(parameters.PublishSettingsFilePath))
            {
                Console.WriteLine("\n{2}. Creating Database {1} on Server {0}", _serverName, parameters.DatabaseName, step);
                ConsoleContinuePrompt("Create");

                Task t = Task.Run(() => controller.CreateDatabaseAsync(_serverName, parameters.DatabaseName, parameters.DatabaseCollation, parameters.DatabaseEdition, parameters.DatabaseMaxSizeInGB));
                WaitForStatus(t);

                Console.WriteLine("\n...Complete");
            }
        }
        private static async Task ConfigureFirewallAsync(SqlManagementControllerParameters parameters, int step)
        {
            using (var controller = new SqlManagementController(parameters.PublishSettingsFilePath))
            {
                Console.WriteLine("\n{1}. Adding Firewall rules for server {0}", _serverName, step);
                ConsoleContinuePrompt("Create");

                Task t = Task.Run(() => controller.ConfigureFirewallAsync(_serverName, parameters.FirewallRuleName, parameters.FirewallRuleStartIP, parameters.FirewallRuleEndIP));
                WaitForStatus(t);

                Console.WriteLine("\n...Complete");
            }
        }
        private static async Task SetupServerAsync(SqlManagementControllerParameters parameters, int step)
        {
            using (var controller = new SqlManagementController(parameters.PublishSettingsFilePath))
            {
                Console.WriteLine("\n{1}. Create logical Server in Region {0}", parameters.ServerRegion, step);
                ConsoleContinuePrompt("Create");

                var t = Task.Run<ServerCreateResponse>(() =>
                {
                    return controller.CreateServerAsync(parameters.ServerRegion, parameters.ServerAdminUsername,
                        parameters.ServerAdminPassword);
                });

                WaitForStatus(t);

                _serverName = t.Result.ServerName;

                if (parameters.FirewallRuleAllowAzureServices)
                {
                    Console.WriteLine("\n{1}. Adding Firewall rules for Azure Services on server {0}", _serverName, step);

                    Task p = Task.Run(() => controller.ConfigureFirewallAsync(_serverName, parameters.FirewallRuleName, parameters.FirewallRuleStartIP, parameters.FirewallRuleEndIP));
                    WaitForStatus(p);
                }

                Console.WriteLine("\n...Complete");
            }
        }

        private static void WaitForStatus(Task t)
        {
            while (t.Status != TaskStatus.RanToCompletion &&
                   t.Status != TaskStatus.Canceled &&
                   t.Status != TaskStatus.Faulted)
            {
                Console.WriteLine(string.Format("\t\t{0}", t.Status));
                Thread.Sleep(5000);
            }
        }

        private static void ConsoleContinuePrompt(string prompt)
        {
            Console.WriteLine("\t > Press Enter to {0}", prompt);
            Console.ReadKey();
            Console.WriteLine("\t\t Starting, view progress in the management portal....");
        }
    }
}

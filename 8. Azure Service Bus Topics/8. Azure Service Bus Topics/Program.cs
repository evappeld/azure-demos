﻿namespace Microsoft.Samples._8.Azure_Service_Bus_Topics
{
    using System;
    using System.Collections.Generic;
    using Microsoft.ServiceBus;
    using Microsoft.ServiceBus.Messaging;
    using System.Threading;

    public class Program
    {
        private static TopicClient topicClient;
        private static string TopicName = "BreakingFashionNews";

        static void Main(string[] args)
        {
            Console.WriteLine("Creating Topic and Subscriptions");
            CreateTopic();
            
            Console.WriteLine("Press anykey to start sending messages ...");
            Console.ReadKey();
            SendMessages();
            Console.WriteLine("Press anykey to start receiving messages that you just sent ...");
            Console.ReadKey();
            ReceiveMessages();
            Console.WriteLine("\nEnd of scenario, press anykey to exit.");
            Console.ReadKey();
        }


        private static void CreateTopic()
        {
            NamespaceManager namespaceManager = NamespaceManager.Create();

            Console.WriteLine("\nCreating Topic " + TopicName + "...");
            try
            {
                if (namespaceManager.TopicExists(TopicName))
                {
                    namespaceManager.DeleteTopic(TopicName);
                }

                TopicDescription myTopic = namespaceManager.CreateTopic(TopicName);

                Console.WriteLine("Creating three Subscriptions ...");
                SubscriptionDescription doutzenSubscription = namespaceManager.CreateSubscription(myTopic.Path, "DoutzenSubscription");
                SubscriptionDescription naomiSubscription = namespaceManager.CreateSubscription(myTopic.Path, "NaomiSubscription");
                SubscriptionDescription claudiaSubscription = namespaceManager.CreateSubscription(myTopic.Path, "ClaudiaSubscription");
            }
            catch (MessagingException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        private static void SendMessages()
        {
            topicClient = TopicClient.Create(TopicName);

            List<BrokeredMessage> messageList = new List<BrokeredMessage>();
            messageList.Add(CreateSampleMessage("1", "New Gucci store opened in Paris"));
            messageList.Add(CreateSampleMessage("2", "Wearing a Blue dress is soooo 2006."));
            messageList.Add(CreateSampleMessage("3", "Check out the new Benetton collection."));

            Console.WriteLine("\nSending messages to topic...");

            foreach (BrokeredMessage message in messageList)
            {
                while (true)
                {
                    try
                    {
                        topicClient.Send(message);
                    }
                    catch (MessagingException e)
                    {
                        if (!e.IsTransient)
                        {
                            Console.WriteLine(e.Message);
                            throw;
                        }
                        else
                        {
                            HandleTransientErrors(e);
                        }
                    }
                    Console.WriteLine(string.Format("Message sent: Id = {0}, Body = {1}", message.MessageId, message.GetBody<string>()));
                    break;
                }
            }

            topicClient.Close();
        }

        private static void ReceiveMessages()
        {
            SubscriptionClient doutzenSubscriptionClient = SubscriptionClient.Create(TopicName, "DoutzenSubscription");
            BrokeredMessage message = null;
            while (true)
            {
                try
                {
                    message = doutzenSubscriptionClient.Receive(TimeSpan.FromSeconds(5));
                    if (message != null)
                    {
                        Console.WriteLine("\nReceiving message from DoutzenSubscription...");
                        Console.WriteLine(string.Format("Message received: Id = {0}, Body = {1}", message.MessageId, message.GetBody<string>()));
                        message.Complete();
                    }
                    else
                    {
                        break;
                    }
                }
                catch (MessagingException e)
                {
                    if (!e.IsTransient)
                    {
                        Console.WriteLine(e.Message);
                        throw;
                    }
                    else
                    {
                        HandleTransientErrors(e);
                    }
                }
            }

            SubscriptionClient claudiaSubscriptionClient = SubscriptionClient.Create(TopicName, "ClaudiaSubscription", ReceiveMode.ReceiveAndDelete);
            while (true)
            {
                try
                {
                    message = claudiaSubscriptionClient.Receive(TimeSpan.FromSeconds(5));
                    if (message != null)
                    {
                        Console.WriteLine("\nReceiving message from ClaudiaSubscription...");
                        Console.WriteLine(string.Format("Message received: Id = {0}, Body = {1}", message.MessageId, message.GetBody<string>()));
                    }
                    else
                    {
                        break;
                    }

                }
                catch (MessagingException e)
                {
                    if (!e.IsTransient)
                    {
                        Console.WriteLine(e.Message);
                        throw;
                    }
                }
            }

            doutzenSubscriptionClient.Close();
            claudiaSubscriptionClient.Close();
        }

        private static BrokeredMessage CreateSampleMessage(string messageId, string messageBody)
        {
            BrokeredMessage message = new BrokeredMessage(messageBody);
            message.MessageId = messageId;
            return message;
        }

        private static void HandleTransientErrors(MessagingException e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine("Will retry sending the message in 2 seconds");
            Thread.Sleep(2000);
        }
    }
}

﻿namespace _4.Azure_Table_storageSample.Model
{
    using Microsoft.WindowsAzure.Storage.Table;

    public class CustomerEntity : TableEntity
    {
        public CustomerEntity() { }

        public CustomerEntity(string lastName, string firstName)
        {
            this.PartitionKey = lastName;
            this.RowKey = firstName;
        }

        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace WcfWorkerRole.Contract
{
    [ServiceContract]
    public interface IProductsService
    {
        [OperationContract]
        [WebGet(UriTemplate="/Products/GetAll", ResponseFormat=WebMessageFormat.Json)]
        ProductData[] GetAll();
    }
}

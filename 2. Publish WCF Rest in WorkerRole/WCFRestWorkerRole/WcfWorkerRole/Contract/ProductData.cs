﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WcfWorkerRole.Contract
{
    [DataContract]
    public class ProductData
    {
        [DataMember]
        public string SKU { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}

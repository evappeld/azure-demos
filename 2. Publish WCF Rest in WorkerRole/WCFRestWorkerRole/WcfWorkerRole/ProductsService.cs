﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WcfWorkerRole.Contract;

namespace WcfWorkerRole
{
    public class ProductsService : IProductsService
    {
        public ProductData[] GetAll()
        {
            return new[] {new ProductData{SKU="1", Name="OS/2 Warp"}};
        }
    }
}

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using System.ServiceModel.Web;
using System.ServiceModel;
using System.Configuration;

namespace WcfWorkerRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private WebServiceHost serviceHost;

        public override void Run()
        {
            Trace.TraceInformation("WcfWorkerRole entry point called", "Information");
            base.Run();
        }

        public override bool OnStart()
        {
            ServicePointManager.DefaultConnectionLimit = 12;
            
            StartWCFService();

            return base.OnStart();
        }

        public override void OnStop()
        {
            Trace.TraceInformation("WcfWorkerRole is stopping");

            base.OnStop();

            Trace.TraceInformation("WcfWorkerRole has stopped");
        }

        private void StartWCFService()
        {
            Trace.TraceInformation("Starting WCF service host...");
            IPEndPoint endpoint = GetInstanceEndpoint("httpIn");
            var address = new Uri(string.Format("http://{0}/", endpoint));

            serviceHost = new WebServiceHost(typeof(ProductsService), address);

            try
            {
                serviceHost.Open();
                Trace.TraceInformation("WCF service host started successfully.");
            }
            catch (TimeoutException timeoutException)
            {
                Trace.TraceError("The service operation timed out. {0}", timeoutException.Message);
            }
            catch (CommunicationException communicationException)
            {
                Trace.TraceError("Could not start WCF service host. {0}", communicationException.Message);
            }
        }

        private static IPEndPoint GetInstanceEndpoint(string endpointName)
        {
            if (RoleEnvironment.IsAvailable)
            {
                return RoleEnvironment.CurrentRoleInstance.InstanceEndpoints[endpointName].IPEndpoint;
            }
            return new IPEndPoint(IPAddress.Loopback, int.Parse(ConfigurationManager.AppSettings[endpointName]));
        }
    }
}

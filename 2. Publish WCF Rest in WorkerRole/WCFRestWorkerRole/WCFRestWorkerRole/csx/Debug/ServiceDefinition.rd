﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="WCFRestWorkerRole" generation="1" functional="0" release="0" Id="319c56ad-196d-4d54-84e7-9d45a9f110be" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="WCFRestWorkerRoleGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="WcfWorkerRole:httpIn" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/LB:WcfWorkerRole:httpIn" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="WcfWorkerRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/MapWcfWorkerRoleInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:WcfWorkerRole:httpIn">
          <toPorts>
            <inPortMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole/httpIn" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapWcfWorkerRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRoleInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="WcfWorkerRole" generation="1" functional="0" release="0" software="D:\projects\Centric\Extra\Azure demos\Publish WCF Rest in WorkerRole\WCFRestWorkerRole\WCFRestWorkerRole\csx\Debug\roles\WcfWorkerRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="httpIn" protocol="http" portRanges="8080" />
            </componentports>
            <settings>
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;WcfWorkerRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;WcfWorkerRole&quot;&gt;&lt;e name=&quot;httpIn&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRoleInstances" />
            <sCSPolicyUpdateDomainMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRoleUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="WcfWorkerRoleUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="WcfWorkerRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="WcfWorkerRoleInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="2756e6a4-0d25-48fd-91a6-caf8a23e9a4b" ref="Microsoft.RedDog.Contract\ServiceContract\WCFRestWorkerRoleContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="b21a9be8-7359-41e8-9384-67b311dddfb3" ref="Microsoft.RedDog.Contract\Interface\WcfWorkerRole:httpIn@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole:httpIn" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>
﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="WCFRestWorkerRole" generation="1" functional="0" release="0" Id="27b0fd8f-1325-4580-b7c0-a7b7b23f2ad0" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="WCFRestWorkerRoleGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="WcfWorkerRole:httpIn" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/LB:WcfWorkerRole:httpIn" />
          </inToChannel>
        </inPort>
        <inPort name="WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" protocol="tcp">
          <inToChannel>
            <lBChannelMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/LB:WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="Certificate|WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" defaultValue="">
          <maps>
            <mapMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/MapCertificate|WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
          </maps>
        </aCS>
        <aCS name="WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" defaultValue="">
          <maps>
            <mapMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/MapWcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" />
          </maps>
        </aCS>
        <aCS name="WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" defaultValue="">
          <maps>
            <mapMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/MapWcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" />
          </maps>
        </aCS>
        <aCS name="WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" defaultValue="">
          <maps>
            <mapMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/MapWcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" />
          </maps>
        </aCS>
        <aCS name="WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" defaultValue="">
          <maps>
            <mapMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/MapWcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" />
          </maps>
        </aCS>
        <aCS name="WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" defaultValue="">
          <maps>
            <mapMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/MapWcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" />
          </maps>
        </aCS>
        <aCS name="WcfWorkerRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/MapWcfWorkerRoleInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:WcfWorkerRole:httpIn">
          <toPorts>
            <inPortMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole/httpIn" />
          </toPorts>
        </lBChannel>
        <lBChannel name="LB:WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput">
          <toPorts>
            <inPortMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole/Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" />
          </toPorts>
        </lBChannel>
        <sFSwitchChannel name="SW:WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp">
          <toPorts>
            <inPortMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole/Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" />
          </toPorts>
        </sFSwitchChannel>
      </channels>
      <maps>
        <map name="MapCertificate|WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" kind="Identity">
          <certificate>
            <certificateMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole/Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
          </certificate>
        </map>
        <map name="MapWcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" kind="Identity">
          <setting>
            <aCSMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole/Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" />
          </setting>
        </map>
        <map name="MapWcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" kind="Identity">
          <setting>
            <aCSMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole/Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" />
          </setting>
        </map>
        <map name="MapWcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" kind="Identity">
          <setting>
            <aCSMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole/Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" />
          </setting>
        </map>
        <map name="MapWcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" kind="Identity">
          <setting>
            <aCSMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole/Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" />
          </setting>
        </map>
        <map name="MapWcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" kind="Identity">
          <setting>
            <aCSMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole/Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" />
          </setting>
        </map>
        <map name="MapWcfWorkerRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRoleInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="WcfWorkerRole" generation="1" functional="0" release="0" software="D:\projects\Centric\Extra\Azure demos\Publish WCF Rest in WorkerRole\WCFRestWorkerRole\WCFRestWorkerRole\csx\Release\roles\WcfWorkerRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="httpIn" protocol="http" portRanges="8080" />
              <inPort name="Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" protocol="tcp" />
              <inPort name="Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" protocol="tcp" portRanges="3389" />
              <outPort name="WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/SW:WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;WcfWorkerRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;WcfWorkerRole&quot;&gt;&lt;e name=&quot;httpIn&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
            <storedcertificates>
              <storedCertificate name="Stored0Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" certificateStore="My" certificateLocation="System">
                <certificate>
                  <certificateMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole/Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
                </certificate>
              </storedCertificate>
            </storedcertificates>
            <certificates>
              <certificate name="Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
            </certificates>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRoleInstances" />
            <sCSPolicyUpdateDomainMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRoleUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="WcfWorkerRoleUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="WcfWorkerRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="WcfWorkerRoleInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="4aab9d9f-8604-4d5d-a58b-d5ef4839937b" ref="Microsoft.RedDog.Contract\ServiceContract\WCFRestWorkerRoleContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="a0621fcd-36c4-45b1-9594-cb29bc81addf" ref="Microsoft.RedDog.Contract\Interface\WcfWorkerRole:httpIn@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole:httpIn" />
          </inPort>
        </interfaceReference>
        <interfaceReference Id="3221b2c0-349d-4739-87b4-1f64acbcb621" ref="Microsoft.RedDog.Contract\Interface\WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/WCFRestWorkerRole/WCFRestWorkerRoleGroup/WcfWorkerRole:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>
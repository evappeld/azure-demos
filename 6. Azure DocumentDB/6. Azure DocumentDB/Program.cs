﻿namespace _6.Azure_DocumentDB
{
    using _6.Azure_DocumentDB.Models;
    using Microsoft.Azure.Documents;
    using Microsoft.Azure.Documents.Client;
    using Microsoft.Azure.Documents.Linq;
    using System;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    class Program
    {
        static readonly string endpoint = ConfigurationManager.AppSettings["endpoint"];
        static readonly string authKey = ConfigurationManager.AppSettings["authKey"];
        static DocumentClient client;

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("1. Create an instance of DocumentClient");
                using (client = new DocumentClient(new Uri(endpoint), authKey))
                {
                    Console.WriteLine("2. Getting reference to Database");
                    Database database = ReadOrCreateDatabase("NForzaDemo");
                   
                    Console.WriteLine("3. Getting reference to a DocumentCollection");
                    DocumentCollection collection = ReadOrCreateCollection(database.SelfLink, "Documents");
                    
                    Console.WriteLine("4. Inserting Documents");
                    CreateDocuments(collection.SelfLink);

                    Console.WriteLine("5. Querying for Documents");
                    QueryDocuments(collection.SelfLink);

                    //Console.WriteLine("6. Cleaning Up");
                    //Cleanup(database.SelfLink);
                }
            }
            catch (DocumentClientException docEx)
            {
                Exception baseException = docEx.GetBaseException();
                Console.WriteLine("{0} StatusCode error occurred with activity id {3}: {1}, Message: {2}",
                    docEx.StatusCode, docEx.Message, baseException.Message, docEx.ActivityId);
            }
            catch (AggregateException aggEx)
            {
                Console.WriteLine("One or more errors occured during execution");
                foreach (var exception in aggEx.InnerExceptions)
                {
                    Console.WriteLine("An exception of type {0} occured: {1}", exception.GetType(), exception.Message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An unexpected exception of type {0} occured: {0}", ex.GetType(), ex.Message);
            }

            Console.WriteLine("\nSample complete. Press any key to exit.");
            Console.ReadKey();
        }

        private static Database ReadOrCreateDatabase(string databaseId)
        {
            var db = client.CreateDatabaseQuery()
                            .Where(d => d.Id == databaseId)
                            .AsEnumerable()
                            .FirstOrDefault();

            if (db == null)
            {
                Console.WriteLine("2. Database not found, creating");
                db = client.CreateDatabaseAsync(new Database { Id = databaseId }).Result;
            }

            return db;
        }

        private static DocumentCollection ReadOrCreateCollection(string databaseLink, string collectionId)
        {
            var col = client.CreateDocumentCollectionQuery(databaseLink)
                              .Where(c => c.Id == collectionId)
                              .AsEnumerable()
                              .FirstOrDefault();

            if (col != null)
            {
                Console.WriteLine("3. Found DocumentCollection.\n3. Deleting DocumentCollection.");
                client.DeleteDocumentCollectionAsync(col.SelfLink).Wait();
            }

            Console.WriteLine("3. Creating DocumentCollection");
            return client.CreateDocumentCollectionAsync(databaseLink, new DocumentCollection { Id = collectionId }).Result;
        }

        private static void CreateDocuments(string collectionLink)
        {
            var task1 = client.CreateDocumentAsync(collectionLink, new Family
            {
                FamilyName = "van Appeldoon",
                Parents = new Parent[]
                {
                    new Parent{FirstName="Erik"}, 
                    new Parent{FirstName="Xiomara"}
                },
                Children = new Child[] 
                {
                    new Child{FirstName="Erikson", Gender="male", Grade=7}
                },
                Pets = new Pet[] 
                { 
                    new Pet{Name="Nacho", Type="Cat"},
                    new Pet{Name="Ricito", Type="Cat"},
                    new Pet{Name="Pinata", Type="Cat"}
                },
                Id = "van Appeldoorn family",
            });

            var task2 = client.CreateDocumentAsync(collectionLink, new Family
            {
                Id = "WakefieldFamily",
                FamilyName = "Wakefield",
                Parents = new Parent[]
                {
                    new Parent{FirstName="Robin"}, 
                    new Parent{FirstName="Ben"}
                },
                Children = new Child[] 
                {
                    new Child{FirstName="Jesse", Gender="female", Grade=1},
                    new Child{FirstName="Lisa", Gender="female", Grade=8}
                },
                Pets = new Pet[] 
                { 
                    new Pet{Name="Goofy", Type="Dog"},
                    new Pet{Name="Shadow", Type="Horse"}
                }
            });

            var task3 = client.CreateDocumentAsync(collectionLink, new Family
            {
                Id = "AdamsFamily",
                FamilyName = "Adams",
                Parents = new Parent[]
                {
                    new Parent{FirstName="Susan"}, 
                },
                Children = new Child[] 
                {
                    new Child{FirstName="Megan", Gender="female"},
                },
            });

            Task.WaitAll(task1, task2, task3);

            Console.WriteLine("4. Documents successfully created");
        }

        private static void QueryDocuments(string collectionLink)
        {
            var result = (from f in client.CreateDocumentQuery<Family>(collectionLink)
                          select f);

            Console.WriteLine(result.AsEnumerable().Count() + " documents found.");

            var query = client.CreateDocumentQuery<Family>(collectionLink).Where(f => f.Id == "van Appeldoorn family");
            Family family = query.AsEnumerable().FirstOrDefault<Family>();
            Console.WriteLine(family.Id);
        }

        private static void Cleanup(string databaseId)
        {
            client.DeleteDatabaseAsync(databaseId).Wait();
        }
    }
}

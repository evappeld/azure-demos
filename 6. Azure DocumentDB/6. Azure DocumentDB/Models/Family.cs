﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;

namespace _6.Azure_DocumentDB.Models
{
    class Family : Document
    {
        public string FamilyName { get; set; }
        public Parent[] Parents { get; set; }
        public Child[] Children { get; set; }
        public Pet[] Pets { get; set; }
    }
}
